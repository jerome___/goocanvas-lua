# Test for LGI Lua library with GooCanvas with item models drawing and moving
_____________________________________________________________________________

## Dependencies

- [Lua version 5.3](https://www.lua.org/docs.html)
- [lgi](https://github.com/pavouk/lgi)

## Wrapper Gtk & GooCanvas referencies

- [GTK3 widgets gallery](https://developer.gnome.org/gtk3/stable/ch03.html)
- [Gtk.Window C API](https://developer.gnome.org/gtk3/stable/GtkWindow.html)
- [Gtk.Frame C API](https://developer.gnome.org/gtk3/stable/GtkFrame.html)
- [Gtk.Statusbar C API](https://developer.gnome.org/gtk3/stable/GtkStatusbar.html)
- [Gtk.Toolbar C API](https://developer.gnome.org/gtk3/stable/GtkToolbar.html)
- [Gtk.ToolButton C API](https://developer.gnome.org/gtk3/stable/GtkToolButton.html)
- [Gtk.Box C API](https://developer.gnome.org/gtk3/stable/GtkBox.html)
- [GooCanvas architecture official C manual](https://developer.gnome.org/goocanvas2/stable/goocanvas-architecture.html)
- [GooCanvas official manual to create items](https://developer.gnome.org/goocanvas/stable/goocanvas-creating-items.html)
- [Goo.Canvas C API](https://developer.gnome.org/goocanvas2/stable/GooCanvas.html)
- [GooCanvas models/views official C example](https://developer.gnome.org/goocanvas2/stable/goocanvas-model-view-canvas.html)
- [GooCanvas official manual coordinates](https://developer.gnome.org/goocanvas2/stable/goocanvas-coordinates.html)
- [Goo.CanvasItemModel C API](https://developer.gnome.org/goocanvas2/stable/GooCanvasItemModel.html)
- [GooCanvasItemModelSimple C API](https://developer.gnome.org/goocanvas2/stable/GooCanvasItemModelSimple.html)
- [Goo.CanvasGroupModel C API](https://developer.gnome.org/goocanvas2/stable/GooCanvasGroupModel.html)
- [Goo.CanvasRectModel C API](https://developer.gnome.org/goocanvas2/stable/GooCanvasRectModel.html)
- [Goo.CanvasTextModel C API](https://developer.gnome.org/goocanvas2/stable/GooCanvasTextModel.html)

## The idea

Read an APi is something, make the link between the ortiginal API and the wrapper to use from is something other. I get hard time to do what i want to do with, i'm not sure 100% that see the best way to go with, but i get a solution. And because i'm sympatic, i want to share with you. Tell me (share) your knowledge and if i can do better with that please.

Show an example to use GooCanvas with items model, events handler for mouse click on item and on created item time.
Show how to flip an item and translate it to let it stay and to be shown on canvas visible area.
Show how to use canvas to item space coordinates from GooCanvas way to go with.
Show how to use models with LGI Lua wrapper library (and specificaly how to assembly parents and child inside a group model tree).

## How to use it

just run the application by:

```sh
./main.lua
```

On left click, the item will flip depend of his make_clickable local flip_options table content.
On right click it will rotate depend of his make_clickable local rotate_options table content.

In the status bar, you will see the flip or rotate resulting item position.
