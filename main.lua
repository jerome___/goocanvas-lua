#! /usr/bin/lua5.3

--------------------------------------------------------------------------------
--         File:  main.lua
--
--        Usage:  ./main.lua
--
--  Description: Transform a rectangle shape by rotate and flip then move
--               it back at initial position
--
--      Options:  absolute flip and rotation and mouse event driven
-- Requirements:  Lua-5.3 and lgi
--         Bugs:  ---
--        Notes:  demonstrate how to use lgi and goo_canvas
--       Author:  Jerome Lanteri<jerome.archlinux@gmail.com>
-- Organization:  
--      Version:  0.8
--      Created:  26/01/2021
--     Revision:  9
--------------------------------------------------------------------------------

local lgi = require 'lgi'
local Goo = lgi.GooCanvas
local Gtk = lgi.Gtk
local Gio = lgi.Gio
local actions = require "actions"


local win = Gtk.Window { width = 640, height = 500, anchor = Gtk.GTK_WINDOW_TOPLEVEL }
local frame = Gtk.Frame { width = 400, height = 400, label = "GooCanvas draw area" }

local canvas = Goo.Canvas { parent = frame, id = 'canvas', width = 300, height = 300 }

function canvas:on_item_created(self)
  print("has been created "..self.model.width)  -- yes, it has been created and i get the model
end

win.on_key_press_event = nil
win.on_map_event = function() win.on_map_event = nil end
win.on_destroy = Gtk.main_quit

tool_bar = Gtk.Toolbar { height = 48 }
tool_bar:insert(Gtk.ToolButton { stock_id = 'gtk-quit', on_clicked = function() win:destroy() end }, -1)
status_bar = Gtk.Statusbar { }

-- Let's create models:
local root_model = Goo.CanvasGroupModel { y = 0, x = 0, width = 500, height = 400 }
local item_model = Goo.CanvasGroupModel { y = 0, x = 0, width = 200, height = 150 }
local rect_model = Goo.CanvasRectModel { x = 0, y = 0, line_width = 2, width = 200, height = 150,
                                         stroke_color = 'yellow', fill_color = 'black', radius_x = 10, radius_y = 10 }
local text_model = Goo.CanvasTextModel { text = 'Voila !', x = 20, y = 20, anchor = Goo.GOO_CANVAS_ANCHOR_CENTER,
                                         font = 'source-code bold 12', fill_color = 'red', width = 100 }
root_model:add_child(item_model, -1)
item_model:add_child(rect_model, -1)
item_model:add_child(text_model, -1)

canvas:set_root_item_model(root_model)    -- define the root item model as root_model to be the CanvasGroupModel, it will create items
--canvas:set_bounds(0, 0, 500, 400)
local item = canvas:get_item(item_model)  -- just get the item from model

local options = { flip =   { "horizontal", "both", "vertical", "no flip", "both", "horizontal", "vertical" },
                  rotate = { "3o clock", "12o clock", "9o clock", "6o clock", "9o clock", "12o clock", "6o clock", "12o clock", "9o clock", "6o clock" }  }
actions:make_clickable(canvas, item, options)                      -- you can make it clickable...

local box = Gtk.Box { orientation = 1, spacing = 5 }
box:add(tool_bar)
box:add(frame)
box:pack_end(status_bar, false, true, 5)

Gtk.Container.add(win, box)

win:show_all()
Gtk.main()
