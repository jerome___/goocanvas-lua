--
--------------------------------------------------------------------------------
--         File:  actions.lua
--
--        Usage:  ./actions.lua
--
--  Description: Actions controller to make item model:
--                _ clickable
--                _ "flipable" on x and y axes
--
--      Options:  ---
-- Requirements:  ---
--         Bugs:  after to flip rotate will show bad item position (wrong translation)
--        Notes:  ---
--       Author:  Jerome Lanteri<jerome.archlinux@gmail.com>
-- Organization:  
--      Version:  0.2
--      Created:  30/01/2021
--     Revision:  4
--------------------------------------------------------------------------------
--

local _ACTIONS_CONTROLLER = {}
local meta = {}

local coordinate_from = { flip = "no flip", rotation = "12o clock" } 
local msg_position = ""


function round(num)
  if num >= 0 then return math.floor(num+.5)
  else return math.ceil(num-.5) end
end

local function show_item_spaces_coordinates(canvas, item, model, condition)
  local cv_x, cv_y = canvas:convert_from_item_space(item, model.x, model.y)
  local it_x, it_y = canvas:convert_to_item_space(item, model.x, model.y)
  print("item coordinates "..condition.." for canvas space: ("..round(cv_x)..","..round(cv_y).."), and for item space: ("..round(it_x)..","..round(it_y)..")")
  if     round(cv_x) == 0 and round(cv_y) == 0 then msg_position = "no flip"; coordinate_from.flip = "no flip";
  elseif round(cv_x) ~= 0 and round(cv_y) == 0 then msg_position = "flip on X axe"
  elseif round(cv_x) == 0 and round(cv_y) ~= 0 then msg_position = "flip on Y axe"
  elseif round(cv_x) ~= 0 and round(cv_y) ~= 0 then msg_position = "flip on X and Y axes" end
end

local function flip(canvas, model, coordinate_to, apply)
  print("-------------------- "..coordinate_from.flip.." to "..coordinate_to.." -------------------------------------")
  if coordinate_to == coordinate_from.flip then goto do_not_flip end
  local item = canvas:get_item(model)
  show_item_spaces_coordinates(canvas, item, model, "before to flip")
  if coordinate_from.flip == "no flip" then
    if     coordinate_to == "both"       then
      model:scale(-1, -1)
      model:translate(-model.width, -model.height)
    elseif coordinate_to == 'horizontal' then
      model:scale(1, -1)
      model:translate(0, -model.height)
    elseif coordinate_to == 'vertical'   then
      model:scale(-1, 1)
      model:translate(-model.width, 0)
    end
  elseif coordinate_from.flip == "horizontal" then
    if     coordinate_to == "both"       then 
      model:scale(-1, 1)
      model:translate(-model.width, 0)
    elseif coordinate_to == 'no flip' then
      model:scale(1, -1)
      model:translate(0, -model.height)
    elseif coordinate_to == 'vertical'   then
      model:scale(-1, -1)
      model:translate(-model.width, -model.height)
    end
  elseif coordinate_from.flip == "vertical" then
    if     coordinate_to == "both"       then
      model:scale(1, -1)
      model:translate(0, -model.height)
    elseif coordinate_to == 'horizontal' then
      model:scale(-1, -1)
      model:translate(-model.width, -model.height)
    elseif coordinate_to == 'no flip'   then
      model:scale(-1, 1)
      model:translate(-model.width, 0)
    end
  elseif coordinate_from.flip == "both" then
    if     coordinate_to == "no flip"       then
      model:scale(-1, -1)
      model:translate(-model.width, -model.height)
    elseif coordinate_to == 'horizontal' then
      model:scale(-1, 1)
      model:translate(-model.width, 0)
    elseif coordinate_to == 'vertical'   then
      model:scale(1, -1)
      model:translate(0, -model.height)
    end
  end
  if apply then coordinate_from.flip = coordinate_to end
  print("define coordinate_from.flip to be: "..coordinate_to.." so that it is: "..coordinate_from.flip)
  show_item_spaces_coordinates(canvas, item, model, "after to translate")
  ::do_not_flip::
end

local function rotate(canvas, model, coordinate_to)
  print("--------------------- "..coordinate_from.rotation.." to "..coordinate_to.." ----------------------------------")
  if coordinate_to == coordinate_from.rotate then goto do_not_rotate end
  local item = canvas:get_item(model)
  show_item_spaces_coordinates(canvas, item, model, "before to rotate")
  local w = model.width
  local h = model.height
  -- should prepare item model to be at no flip position to rotate item model on next step
  if coordinate_from.rotation == "3o clock" then
    if     coordinate_to == "6o clock"  then 
      model:rotate(-90, h/2, h/2)
      model:rotate(180, h/2, w/2)
    elseif coordinate_to == "9o clock"  then model:rotate(180, h/2, w/2)
    elseif coordinate_to == "12o clock" then model:rotate(270, h/2, h/2)
    end
  elseif coordinate_from.rotation == "6o clock" then
    if     coordinate_to == "9o clock"  then model:rotate(90, w-(h/2), h/2 )
    elseif coordinate_to == "12o clock" then model:rotate(180, w/2, h/2)
    elseif coordinate_to == "3o clock"  then 
      model:rotate(180, w/2, h/2)
      model:rotate(90, h/2, h/2)
    end
  elseif coordinate_from.rotation == "9o clock" then
    if     coordinate_to == "12o clock" then 
      model:rotate(180, w/2, h/2)
      model:rotate(-90, h/2, h/2)
    elseif coordinate_to == "3o clock"  then model:rotate(180, w/2, h/2)
    elseif coordinate_to == "6o clock"  then model:rotate(270, w - (h/2), h/2)
    end
  elseif coordinate_from.rotation == "12o clock" then
    if     coordinate_to == "3o clock" then model:rotate(90, h/2, h/2)
    elseif coordinate_to == "6o clock" then model:rotate(180, w/2, h/2)
    elseif coordinate_to == "9o clock" then
      model:rotate(90, h/2, h/2)
      model:rotate(180, w/2, h/2)
    end
  end
  -- should flip back item model to initial input flip state.
  show_item_spaces_coordinates(canvas, item, model, "after to rotate")
  coordinate_from.rotation = coordinate_to
  show_item_spaces_coordinates(canvas, item, model, "after to translate")
  ::do_not_rotate::
end

function _ACTIONS_CONTROLLER.make_clickable(self, canvas, item, options)
  self.canvas = canvas
  self.item = item
  self.options = options
  local model = item:get_model()
  local give_flip_option = coroutine.create( 
    function(options) 
      while true do 
        for _, option in ipairs(options) do coroutine.yield(option) end 
      end 
    end )
  local give_rotate_option = coroutine.create( 
    function(options) 
      while true do 
        for _, option in ipairs(options) do coroutine.yield(option) end 
      end 
    end )
  function item:on_button_press_event(it, event)
    if event.button == 1 then -- left click
      local running, option = coroutine.resume(give_flip_option, options.flip)
      if running then
        flip(canvas, model, option, true)
        status_bar:push(1, "Left button pressed inside item. Option is \""..option.."\" and result item position is: "..msg_position)
      end
    elseif event.button == 3 then --right click
      local running, option = coroutine.resume(give_rotate_option, options.rotate)
      if running then
        rotate(canvas, model, option)
        status_bar:push(1, "Right button pressed inside item. Option is \""..option.."\" and result item position is: "..msg_position)
      end
    end
  end
  return setmetatable({ item = self.item, options = self.options }, meta)
 end

 meta.__index = _ACTIONS_CONTROLLER

 return _ACTIONS_CONTROLLER
